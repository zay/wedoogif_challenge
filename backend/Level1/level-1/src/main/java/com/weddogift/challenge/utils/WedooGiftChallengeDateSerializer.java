package com.weddogift.challenge.utils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.util.Date;

public class WedooGiftChallengeDateSerializer extends StdSerializer<Date> {

    public WedooGiftChallengeDateSerializer() {
        this(null);
    }

    public WedooGiftChallengeDateSerializer(Class<Date> t) {
        super(t);
    }

    @Override
    public void serialize(
            Date value, JsonGenerator gen, SerializerProvider arg2)
            throws IOException, JsonProcessingException {
        gen.writeString(Constants.DF.format(value));
    }
}

package com.weddogift.challenge.entities;


import lombok.Data;

@Data
public class GenericEntity  {
    private Long id;
}

package com.weddogift.challenge.entities;

import lombok.Data;

@Data
public class User extends GenericEntity {
    private Double balance = 0.0;
}

package com.weddogift.challenge.dao;

import com.weddogift.challenge.entities.User;
import org.springframework.stereotype.Repository;

@Repository
public class UserDao extends GenericDaoImpl<User>{
}

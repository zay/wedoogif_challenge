package com.weddogift.challenge.services;


import com.weddogift.challenge.dto.UserDto;

import java.util.List;

public interface UserService {
        UserDto calculateBalanceByUserId(Long userId);
        List<UserDto> findAll();
}

package com.weddogift.challenge.dao;


import com.weddogift.challenge.entities.GenericEntity;

import java.util.List;
import java.util.Optional;

public interface GenericDao<U extends GenericEntity> {

    U add(U u);
    List<U> findAll();
    Optional<U> findById(Long id);
    void deleteAll();
}

package com.weddogift.challenge.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan(basePackages = {"com.weddogift.challenge.dao",
        "com.weddogift.challenge.services",
        "com.weddogift.challenge.controllers"})
@Configuration
public class ApplicationContextConfig {

    @Bean
    public ModelMapper modelMapper(){
        return new ModelMapper();
    }
}

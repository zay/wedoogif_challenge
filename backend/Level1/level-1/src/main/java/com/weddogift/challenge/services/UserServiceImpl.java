package com.weddogift.challenge.services;


import com.weddogift.challenge.dao.DistributionDao;
import com.weddogift.challenge.dao.UserDao;
import com.weddogift.challenge.dto.UserDto;
import com.weddogift.challenge.entities.Distribution;
import com.weddogift.challenge.entities.User;
import com.weddogift.challenge.exceptions.DataNotFoundException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LogManager.getLogger(UserServiceImpl.class);

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private DistributionDao distributionDao;

    @Autowired
    private UserDao userDao;

    @Override
    public UserDto calculateBalanceByUserId(Long userId) {
        LOGGER.debug("calculateBalanceByUserId() is called");

        User user = userDao.findById(userId).orElseThrow(() -> new DataNotFoundException("User not found"));
        List<Distribution> distributions = this.distributionDao.findByUserId(userId);
        Double distributionAmount = distributions.stream().filter(d -> d.getEndDate().after(new Date()))
                .map(Distribution::getAmount)
                .reduce(Double::sum)
                .orElse(0.0);
        user.setBalance(user.getBalance() + distributionAmount);
        return this.modelMapper.map(user, UserDto.class);
    }

    @Override
    public List<UserDto> findAll() {
        LOGGER.debug("findAll() is called");
        return this.userDao.findAll().stream()
                .map(u -> this.modelMapper.map(u, UserDto.class))
                .collect(Collectors.toList());
    }
}

package com.weddogift.challenge.services;


import com.weddogift.challenge.dto.CompanyDto;

import java.util.List;

public interface CompanyService {
    List<CompanyDto> findAll();
}

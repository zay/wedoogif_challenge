package com.weddogift.challenge.controllers;

import com.weddogift.challenge.dto.DistributionDto;
import com.weddogift.challenge.services.DistributionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("distribution")
public class DistributionController {

    @Autowired
    private DistributionService distributionService;

    @PostMapping("/")
    public DistributionDto createDistribution(@RequestBody DistributionDto dto){
        return distributionService.createGiftDistribution(dto.getCompanyId(), dto.getUserId(), dto.getStartDate(), dto.getAmount());
    }

}

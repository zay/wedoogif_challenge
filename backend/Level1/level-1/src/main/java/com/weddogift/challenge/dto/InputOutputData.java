package com.weddogift.challenge.dto;

import lombok.Data;

import java.util.List;

@Data
public class InputOutputData {
    private List<CompanyDto> companies;
    private List<UserDto> users;
    private List<DistributionDto> distributions;
}

package com.weddogift.challenge.services;

import com.weddogift.challenge.dao.CompanyDao;
import com.weddogift.challenge.dao.DistributionDao;
import com.weddogift.challenge.dao.UserDao;
import com.weddogift.challenge.dto.DistributionDto;
import com.weddogift.challenge.entities.Company;
import com.weddogift.challenge.entities.Distribution;
import com.weddogift.challenge.exceptions.DataNotFoundException;
import com.weddogift.challenge.exceptions.OperationNotAllowedException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DistributionServiceImpl implements DistributionService {

    private static final Logger LOGGER = LogManager.getLogger(DistributionServiceImpl.class);

    @Autowired
    private CompanyDao companyDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private DistributionDao distributionDao;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public DistributionDto createGiftDistribution(Long companyId, Long userId, Date startDate, Double amount) {
        LOGGER.debug("createGiftDistribution() is called");
        Company company = this.companyDao.findById(companyId).orElseThrow(DataNotFoundException::new);
        if (company.getBalance() >= amount) {
            company.setBalance(company.getBalance() - amount);
            Distribution distribution = new Distribution();
            distribution.setAmount(amount);
            distribution.setCompany(companyDao.findById(companyId).get());
            distribution.setUser(this.userDao.findById(userId).get());
            distribution.setStartDate(startDate);
            Calendar c = Calendar.getInstance();
            c.setTime(startDate);
            c.add(Calendar.YEAR, 1);
            c.add(Calendar.DAY_OF_YEAR,-1 );
            distribution.setEndDate(c.getTime());
            this.distributionDao.add(distribution);
            LOGGER.debug("createDistribution() is ended");
            return this.modelMapper.map(distribution, DistributionDto.class);
        } else {
            LOGGER.error("Not enough balance for company exception");
            throw new OperationNotAllowedException("Not enough balance for company exception");
        }
    }

    @Override
    public List<DistributionDto> findAll() {
        LOGGER.debug("findAll() is called");
        return this.distributionDao.findAll().stream()
                .map(d -> this.modelMapper.map(d, DistributionDto.class))
                .collect(Collectors.toList());
    }
}

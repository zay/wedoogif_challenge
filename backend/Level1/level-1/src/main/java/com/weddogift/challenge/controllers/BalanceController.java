package com.weddogift.challenge.controllers;

import com.weddogift.challenge.dto.UserDto;
import com.weddogift.challenge.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("balance")
public class BalanceController {

    @Autowired
    private UserService userService;

    @GetMapping("/{user_id}")
    public UserDto getUserBalance(@PathVariable("user_id") Long userId){
        return this.userService.calculateBalanceByUserId(userId);
    }

    @GetMapping("/")
    public List<UserDto> getAllUsersBalance(){
            return this.userService.findAll().stream().map(u -> this.userService.calculateBalanceByUserId(u.getId())).collect(Collectors.toList());
    }
}

package com.weddogift.challenge.application;

import com.weddogift.challenge.config.ApplicationContextConfig;
import com.weddogift.challenge.config.DataInit;
import com.weddogift.challenge.security.SecurityConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication( scanBasePackageClasses = {ApplicationContextConfig.class, DataInit.class, SecurityConfig.class})
public class Level1Application {
    public static void main(String[] args) {
        SpringApplication.run(Level1Application.class, args);
    }
}

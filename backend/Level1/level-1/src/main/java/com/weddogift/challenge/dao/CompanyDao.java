package com.weddogift.challenge.dao;

import com.weddogift.challenge.entities.Company;
import org.springframework.stereotype.Repository;

@Repository
public class CompanyDao extends GenericDaoImpl<Company> {
}

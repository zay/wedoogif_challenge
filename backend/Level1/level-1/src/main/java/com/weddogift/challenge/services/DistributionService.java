package com.weddogift.challenge.services;

import com.weddogift.challenge.dto.DistributionDto;

import java.util.Date;
import java.util.List;

public interface DistributionService {


    DistributionDto createGiftDistribution(Long companyId, Long userId, Date startDate, Double amount);

    List<DistributionDto> findAll();

}

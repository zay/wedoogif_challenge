package com.weddogift.challenge.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class UserDto {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("balance")
    private Double balance;
}

package com.weddogift.challenge.entities;

import lombok.Data;

@Data
public class Company extends GenericEntity {

    private String name;
    private Double balance;

}

package com.wedoogift.challenge.test.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.weddogift.challenge.dao.CompanyDao;
import com.weddogift.challenge.dao.UserDao;
import com.weddogift.challenge.entities.Company;
import com.weddogift.challenge.entities.User;
import com.weddogift.challenge.dto.InputOutputData;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.io.IOException;

@Configuration
public class TestApplicationContextConfig {

    public static final String LEVEL1_INPUT_PATH = "../data/input.json";
    public static final String LEVEL1_OUTPUT_PATH = "../data/output.json";


    private static final Logger LOGGER = LogManager.getLogger(TestApplicationContextConfig.class);

    @Autowired
    private UserDao userDao;

    @Autowired
    private CompanyDao companyDao;

    @Autowired
    private ModelMapper modelMapper;

    @Bean
    public void initLevel1Data() throws IOException {
        LOGGER.info("Start loading data for level 1 from "+LEVEL1_INPUT_PATH);
        File file = new File(LEVEL1_INPUT_PATH);
        ObjectMapper mapper = new ObjectMapper();
        try {
            InputOutputData dataInput = mapper.readValue(file, InputOutputData.class);
            dataInput.getCompanies().forEach(c -> this.companyDao.add(this.modelMapper.map(c, Company.class)));
            dataInput.getUsers().forEach(u -> this.userDao.add(this.modelMapper.map(u, User.class)));
            LOGGER.info("End loading data");
        } catch (IOException e) {
            LOGGER.error("Error loading file "+ LEVEL1_INPUT_PATH, e);
            throw  e;
        }

    }

}

package com.wedoogift.challenge.test;


import com.fasterxml.jackson.databind.ObjectMapper;

import com.weddogift.challenge.config.ApplicationContextConfig;
import com.weddogift.challenge.services.CompanyService;
import com.weddogift.challenge.services.DistributionService;
import com.weddogift.challenge.services.UserService;
import com.weddogift.challenge.utils.Constants;
import com.wedoogift.challenge.test.config.TestApplicationContextConfig;
import com.weddogift.challenge.dto.InputOutputData;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {TestApplicationContextConfig.class, ApplicationContextConfig.class})
public class TestLevel1 {

    private static final Logger LOGGER = LogManager.getLogger(TestLevel1.class);


    @Autowired
    private CompanyService companyService;

    @Autowired
    private DistributionService distributionService;

    @Autowired
    private UserService userService;

    public void createDistributionTest() throws ParseException {
        LOGGER.info("level-1 createDistributionTest Start");
        this.distributionService.createGiftDistribution(1L, 1L, Constants.DF.parse("2020-09-16"), 50.0);
        this.distributionService.createGiftDistribution(1L, 2L, Constants.DF.parse("2020-08-01"), 100.0);
        this.distributionService.createGiftDistribution(2L, 3L, Constants.DF.parse("2020-05-01"), 1000.0);
        LOGGER.info("level-1 createDistributionTest End");
    }

    public void calculateUserDistributionTest() {
        LOGGER.info("level-1 calculateUserDistributionTest Start");
        userService.findAll().forEach(u -> {
            this.userService.calculateBalanceByUserId(u.getId());
        });
        LOGGER.info("level-1 calculateUserDistributionTest End");
    }

    @Test
    public void level1Test() throws IOException, ParseException {

        createDistributionTest();
        calculateUserDistributionTest();

        InputOutputData inputOutputData = new InputOutputData();

        inputOutputData.setCompanies(this.companyService.findAll());
        inputOutputData.setDistributions(this.distributionService.findAll());
        inputOutputData.setUsers(this.userService.findAll());

        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(new File(TestApplicationContextConfig.LEVEL1_OUTPUT_PATH), inputOutputData);
        } catch (IOException e) {
            LOGGER.error("error writing file " + TestApplicationContextConfig.LEVEL1_OUTPUT_PATH);
            throw e;
        }
    }
}

package com.wedoogift.challenge.dao;

import com.wedoogift.challenge.entities.Distribution;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class DistributionDao extends GenericDaoImpl<Distribution> {

    public List<Distribution> findByUserId(Long userId){
        return this.entities.stream().filter(e -> e.getUser().getId().equals(userId)).collect(Collectors.toList());
    }
}

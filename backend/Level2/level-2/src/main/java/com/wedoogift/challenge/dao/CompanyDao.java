package com.wedoogift.challenge.dao;

import com.wedoogift.challenge.entities.Company;
import org.springframework.stereotype.Repository;

@Repository
public class CompanyDao extends GenericDaoImpl<Company> {
}

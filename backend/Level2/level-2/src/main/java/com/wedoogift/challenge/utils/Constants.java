package com.wedoogift.challenge.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class Constants {

    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final DateFormat DF = new SimpleDateFormat(DATE_FORMAT);

    private Constants() {
    }
}

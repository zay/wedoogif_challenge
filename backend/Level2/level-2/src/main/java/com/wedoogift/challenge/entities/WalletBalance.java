package com.wedoogift.challenge.entities;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WalletBalance {

    private Double amount;
    private Long walletId;

}

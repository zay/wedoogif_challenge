package com.wedoogift.challenge.entities;

public enum WalletType {
    GIFT, FOOD
}

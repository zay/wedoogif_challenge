package com.wedoogift.challenge.exceptions;

public class OperationNotAllowedException extends RuntimeException {
    public OperationNotAllowedException() {
    }

    public OperationNotAllowedException(String message) {
        super(message);
    }
}

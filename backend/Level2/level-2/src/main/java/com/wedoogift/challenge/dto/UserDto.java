package com.wedoogift.challenge.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class UserDto {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("balance")
    private List<WalletBalanceDto> balance;
}

package com.wedoogift.challenge.dto;

import com.wedoogift.challenge.entities.WalletType;
import lombok.Data;

@Data
public class WalletDto {

    private Long id;
    private String name;
    private WalletType type;
}

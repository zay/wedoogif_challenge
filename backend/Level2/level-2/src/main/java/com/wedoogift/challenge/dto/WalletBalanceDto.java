package com.wedoogift.challenge.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class WalletBalanceDto {

    @JsonProperty("amount")
    private Double amount;

    @JsonProperty("wallet_id")
    private Long walletId;
}

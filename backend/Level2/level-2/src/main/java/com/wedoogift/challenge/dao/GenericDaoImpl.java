package com.wedoogift.challenge.dao;

import com.wedoogift.challenge.entities.GenericEntity;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class GenericDaoImpl<U extends GenericEntity> implements GenericDao<U> {


    protected List<U> entities = new ArrayList<>();

    @Override
    public U add(U u) {
        u.setId((long) entities.size() + 1);
        this.entities.add(u);
        return u;
    }

    @Override
    public List<U> findAll() {
        return this.entities;
    }

    @Override
    public Optional<U> findById(Long id) {
        return this.entities.stream().filter(u -> u.getId().equals(id)).findAny();
    }

    @Override
    public void deleteAll() {
        entities = new ArrayList<>();
    }
}

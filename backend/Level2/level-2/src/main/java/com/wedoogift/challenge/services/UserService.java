package com.wedoogift.challenge.services;

import com.wedoogift.challenge.dto.UserDto;

import java.util.List;

public interface UserService {
        void calculateBalanceByUserId(Long userId);
        List<UserDto> findAll();
}

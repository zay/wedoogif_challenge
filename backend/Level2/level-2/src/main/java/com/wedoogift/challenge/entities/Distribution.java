package com.wedoogift.challenge.entities;

import lombok.Data;

import java.util.Date;

@Data
public class Distribution extends GenericEntity  {
    private Double amount;
    private Date startDate;
    private Date endDate;
    private Company company;
    private User user;
    private Wallet wallet;
}

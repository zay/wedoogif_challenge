package com.wedoogift.challenge.services;

import com.wedoogift.challenge.dao.CompanyDao;
import com.wedoogift.challenge.dao.DistributionDao;
import com.wedoogift.challenge.dao.UserDao;
import com.wedoogift.challenge.dao.WalletDao;
import com.wedoogift.challenge.dto.DistributionDto;
import com.wedoogift.challenge.entities.Company;
import com.wedoogift.challenge.entities.Distribution;
import com.wedoogift.challenge.entities.Wallet;
import com.wedoogift.challenge.entities.WalletType;
import com.wedoogift.challenge.exceptions.OperationNotAllowedException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DistributionServiceImpl implements DistributionService {

    private static final Logger LOGGER = LogManager.getLogger(DistributionServiceImpl.class);


    @Autowired
    private DistributionDao distributionDao;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private CompanyDao companyDao;

    @Autowired
    private UserDao level2UserDao;

    @Autowired
    private WalletDao walletDao;


    @Override
    public void createGiftDistribution(Long companyId, Long userId, Date startDate, Double amount) {
        LOGGER.debug("level-2 createGiftDistribution() is called");
        Company company = this.companyDao.findById(companyId).get();
        Wallet wallet = this.walletDao.findByType(WalletType.GIFT).get();
        if (company.getBalance() >= amount) {
            company.setBalance(company.getBalance() - amount);
            Distribution distribution = new Distribution();
            distribution.setAmount(amount);
            distribution.setCompany(companyDao.findById(companyId).get());
            distribution.setUser(this.level2UserDao.findById(userId).get());
            distribution.setStartDate(startDate);
            distribution.setWallet(wallet);
            Calendar c = Calendar.getInstance();
            c.setTime(startDate);
            c.add(Calendar.YEAR, 1);
            c.add(Calendar.DAY_OF_YEAR, -1);
            distribution.setEndDate(c.getTime());
            this.distributionDao.add(distribution);
        } else {
            LOGGER.error("Not enough balance for company exception");
            throw new OperationNotAllowedException("Not enough balance for company exception");
        }
        LOGGER.debug("level-2 createGiftDistribution() is ended");
    }

    @Override
    public void createFoodDistribution(Long companyId, Long userId, Date startDate, Double amount) {
        LOGGER.debug("level-2 createFoodDistribution() is called");
        Company company = this.companyDao.findById(companyId).get();
        Wallet wallet = this.walletDao.findByType(WalletType.FOOD).get();
        if (company.getBalance() >= amount) {
            company.setBalance(company.getBalance() - amount);
            Distribution distribution = new Distribution();
            distribution.setAmount(amount);
            distribution.setCompany(companyDao.findById(companyId).get());
            distribution.setUser(this.level2UserDao.findById(userId).get());
            distribution.setStartDate(startDate);
            distribution.setWallet(wallet);

            Calendar c =  Calendar.getInstance();
            c.setTime(startDate);
            c.add(Calendar.YEAR, 1);
            c.set(Calendar.MONTH, 1);
            c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DATE));


            distribution.setEndDate(c.getTime());
            this.distributionDao.add(distribution);
        } else {
             LOGGER.error("Not enough balance for company exception");
             throw new OperationNotAllowedException("Not enough balance for company exception");
        }
        LOGGER.debug("level-2 createGiftDistribution() is ended");
    }

    @Override
    public List<DistributionDto> findAll() {
        LOGGER.debug("findAll() is called");
        return this.distributionDao.findAll().stream()
                .map(d -> this.modelMapper.map(d, DistributionDto.class))
                .collect(Collectors.toList());
    }
}

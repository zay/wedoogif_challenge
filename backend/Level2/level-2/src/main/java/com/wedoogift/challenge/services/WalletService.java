package com.wedoogift.challenge.services;

import com.wedoogift.challenge.dto.WalletDto;

import java.util.List;

public interface WalletService  {

    List<WalletDto> findAll();
}

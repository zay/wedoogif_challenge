package com.wedoogift.challenge.services;

import com.wedoogift.challenge.dao.CompanyDao;
import com.wedoogift.challenge.dto.CompanyDto;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CompanyServiceImpl implements CompanyService {

    private static final Logger LOGGER = LogManager.getLogger(CompanyServiceImpl.class);

    @Autowired
    private CompanyDao companyDao;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<CompanyDto> findAll() {
        LOGGER.debug("findAll() is called");
        return this.companyDao.findAll().stream()
        .map(c -> this.modelMapper.map(c, CompanyDto.class))
        .collect(Collectors.toList());
    }
}

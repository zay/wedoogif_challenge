package com.wedoogift.challenge.services;

import com.wedoogift.challenge.dto.CompanyDto;

import java.util.List;

public interface CompanyService {
    List<CompanyDto> findAll();
}

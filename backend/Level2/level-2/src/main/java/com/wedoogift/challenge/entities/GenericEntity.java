package com.wedoogift.challenge.entities;


import lombok.Data;

@Data
public class GenericEntity  {
    private Long id;
}

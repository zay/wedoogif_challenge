package com.wedoogift.challenge.config;


import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan(basePackages = {"com.wedoogift.challenge.dao",
        "com.wedoogift.challenge.services"})
@Configuration
public class ApplicationContextConfig {

    @Bean
    public ModelMapper modelMapper(){
        return new ModelMapper();
    }
}

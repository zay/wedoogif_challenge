package com.wedoogift.challenge.dao;

import com.wedoogift.challenge.entities.GenericEntity;

import java.util.List;
import java.util.Optional;

public interface GenericDao<U extends GenericEntity> {

    U add(U u);
    List<U> findAll();
    Optional<U> findById(Long id);
    void deleteAll();
}

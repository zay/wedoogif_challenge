package com.wedoogift.challenge.exceptions;

public class DataNotFoundException extends RuntimeException {


    public DataNotFoundException() {
        super();
    }

    public DataNotFoundException(String message) {
        super(message);
    }
}

package com.wedoogift.challenge.dao;

import com.wedoogift.challenge.entities.User;
import org.springframework.stereotype.Repository;

@Repository
public class UserDao extends GenericDaoImpl<User> {
}

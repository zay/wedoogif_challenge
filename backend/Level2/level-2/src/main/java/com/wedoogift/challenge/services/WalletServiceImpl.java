package com.wedoogift.challenge.services;

import com.wedoogift.challenge.dao.WalletDao;
import com.wedoogift.challenge.dto.WalletDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

public class WalletServiceImpl implements WalletService {

    @Autowired
    private WalletDao walletDao;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<WalletDto> findAll() {
        return this.walletDao.findAll().stream().map(w -> this.modelMapper.map(w, WalletDto.class)).collect(Collectors.toList());
    }
}

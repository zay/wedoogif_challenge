package com.wedoogift.challenge.entities;

import lombok.Data;

@Data
public class Wallet extends GenericEntity {
    private WalletType type;
}

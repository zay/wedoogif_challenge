package com.wedoogift.challenge.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.wedoogift.challenge.utils.Constants;
import com.wedoogift.challenge.utils.WedooGiftChallengeDateSerializer;
import lombok.Data;

import java.util.Date;

@Data
public class DistributionDto {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("amount")
    private Double amount;

    @JsonProperty("start_date")
    @JsonSerialize(using = WedooGiftChallengeDateSerializer.class)
    private Date startDate;

    @JsonProperty("end_date")
    @JsonSerialize(using = WedooGiftChallengeDateSerializer.class)
    private Date endDate;

    @JsonProperty("company_id")
    private Long companyId;

    @JsonProperty("user_id")
    private Long userId;

    @JsonProperty("wallet_id")
    private Long walletId;
}

package com.wedoogift.challenge.entities;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class User extends GenericEntity{
    private List<WalletBalance> balance  = new ArrayList<>();
}

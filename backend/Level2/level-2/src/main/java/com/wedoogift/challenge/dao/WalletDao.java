package com.wedoogift.challenge.dao;

import com.wedoogift.challenge.entities.Wallet;
import com.wedoogift.challenge.entities.WalletType;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class WalletDao extends GenericDaoImpl<Wallet> {

    public Optional<Wallet> findByType(WalletType type){
        return this.entities.stream().filter(w -> w.getType().equals(type)).findAny();
    }
}

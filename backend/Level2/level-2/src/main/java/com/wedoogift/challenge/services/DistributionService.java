package com.wedoogift.challenge.services;

import com.wedoogift.challenge.dto.DistributionDto;

import java.util.Date;
import java.util.List;

public interface DistributionService {


    void createGiftDistribution(Long companyId, Long userId, Date startDate, Double amount);
    void createFoodDistribution(Long companyId, Long userId, Date startDate, Double amount);


    List<DistributionDto> findAll();

}

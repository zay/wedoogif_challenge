package com.wedoogift.challenge.services;

import com.wedoogift.challenge.dao.DistributionDao;
import com.wedoogift.challenge.dao.UserDao;
import com.wedoogift.challenge.dao.WalletDao;
import com.wedoogift.challenge.dto.UserDto;
import com.wedoogift.challenge.entities.*;
import com.wedoogift.challenge.exceptions.DataNotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private DistributionDao distributionDao;

    @Autowired
    private WalletDao walletDao;


    @Override
    public void calculateBalanceByUserId(Long userId) {
        User user = this.userDao.findById(userId).orElseThrow(() -> new DataNotFoundException("User not found"));
        Wallet giftWallet = this.walletDao.findByType(WalletType.GIFT).orElseThrow(()-> new DataNotFoundException("Gift wallet not found"));
        Wallet foodWallet = this.walletDao.findByType(WalletType.FOOD).orElseThrow(()-> new DataNotFoundException("Food wallet not found"));

        List<Distribution> distributions = this.distributionDao.findByUserId(userId);

        Optional<Double> giftDistributionAmount  = distributions.stream()
                .filter(d -> d.getWallet().getType().equals(WalletType.GIFT))
                .filter(d -> d.getEndDate().after(new Date()))
                .map(Distribution::getAmount)
                .reduce(Double::sum);

        Optional<Double> foodDistributionAmount  = distributions.stream()
                .filter(d -> d.getWallet().getType().equals(WalletType.FOOD))
                .filter(d -> d.getEndDate().after(new Date()))
                .map(Distribution::getAmount)
                .reduce(Double::sum);

        Optional<WalletBalance> giftBalance =  user.getBalance().stream()
                .filter(w -> w.getWalletId().equals(giftWallet.getId()))
                .findAny();

        Optional<WalletBalance> foodBalance =  user.getBalance().stream()
                .filter(w -> w.getWalletId().equals(foodWallet.getId()))
                .findAny();

        if(giftBalance.isPresent()){
            giftBalance.get().setAmount(giftBalance.get().getAmount() + giftDistributionAmount.orElse(0.0));
        } else {
            giftDistributionAmount.ifPresent(aDouble -> user.getBalance().add(new WalletBalance(aDouble, giftWallet.getId())));
        }

        if(foodBalance.isPresent()){
            foodBalance.get().setAmount(foodBalance.get().getAmount() + foodDistributionAmount.orElse(0.0));
        } else {
            foodDistributionAmount.ifPresent(aDouble -> user.getBalance().add(new WalletBalance(aDouble, giftWallet.getId())));
        }

    }

    @Override
    public List<UserDto> findAll() {
        return this.userDao.findAll().stream()
                .map(u -> this.modelMapper.map(u, UserDto.class))
                .collect(Collectors.toList());
    }
}

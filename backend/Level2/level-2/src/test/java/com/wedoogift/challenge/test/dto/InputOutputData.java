package com.wedoogift.challenge.test.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.wedoogift.challenge.dto.CompanyDto;
import com.wedoogift.challenge.dto.DistributionDto;
import com.wedoogift.challenge.dto.UserDto;
import com.wedoogift.challenge.dto.WalletDto;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class InputOutputData {

    private List<WalletDto> wallets;
    private List<CompanyDto> companies;
    private List<UserDto> users;
    private List<DistributionDto> distributions;

}

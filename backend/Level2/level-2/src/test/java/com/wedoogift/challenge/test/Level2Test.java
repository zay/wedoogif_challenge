package com.wedoogift.challenge.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wedoogift.challenge.config.ApplicationContextConfig;
import com.wedoogift.challenge.services.CompanyService;
import com.wedoogift.challenge.services.DistributionService;
import com.wedoogift.challenge.services.UserService;
import com.wedoogift.challenge.test.config.TestApplicationContextConfig;
import com.wedoogift.challenge.test.dto.InputOutputData;
import com.wedoogift.challenge.utils.Constants;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {TestApplicationContextConfig.class, ApplicationContextConfig.class})
public class Level2Test {

    private static final Logger LOGGER = LogManager.getLogger(Level2Test.class);


    @Autowired
    private DistributionService distributionService;

    @Autowired
    private UserService userService;

    @Autowired
    private CompanyService companyService;

    private void testCreateGiftDistributionForUsers() throws ParseException {
        this.distributionService.createGiftDistribution(1L, 1L, Constants.DF.parse("2020-09-16"), 50.0);
        this.distributionService.createGiftDistribution(1L, 2L, Constants.DF.parse("2020-08-01"), 100.0);
        this.distributionService.createGiftDistribution(2L, 3L, Constants.DF.parse("2020-05-01"), 1000.0);
    }

    private void testCreateFoodDistributionForUsers() throws ParseException {
        this.distributionService.createFoodDistribution(1L, 1L, Constants.DF.parse("2023-05-01"), 250.0);

    }

    private void testCalculateUserBalanceByUserId(){
        this.userService.findAll().forEach(u-> this.userService.calculateBalanceByUserId(u.getId()));
    }
    @Test
    public void level2Test() throws ParseException, IOException {
        testCreateGiftDistributionForUsers();
        testCreateFoodDistributionForUsers();
        testCalculateUserBalanceByUserId();

        InputOutputData inputOutputData = new InputOutputData();

        inputOutputData.setCompanies(this.companyService.findAll());
        inputOutputData.setDistributions(this.distributionService.findAll());
        inputOutputData.setUsers(this.userService.findAll());

        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(new File(TestApplicationContextConfig.LEVEL2_OUTPUT_PATH), inputOutputData);
        } catch (IOException e) {
            LOGGER.error("error writing file " + TestApplicationContextConfig.LEVEL2_OUTPUT_PATH);
            throw e;
        }
    }



}

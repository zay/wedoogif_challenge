package com.wedoogift.challenge.test.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wedoogift.challenge.dao.CompanyDao;
import com.wedoogift.challenge.dao.UserDao;
import com.wedoogift.challenge.dao.WalletDao;
import com.wedoogift.challenge.entities.Company;
import com.wedoogift.challenge.entities.User;
import com.wedoogift.challenge.entities.Wallet;
import com.wedoogift.challenge.test.dto.InputOutputData;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.io.IOException;

@Configuration
public class TestApplicationContextConfig {

    public static final String LEVEL2_INPUT_PATH = "../data/input.json";
    public static final String LEVEL2_OUTPUT_PATH = "../data/output.json";


    private static final Logger LOGGER = LogManager.getLogger(TestApplicationContextConfig.class);

    @Autowired
    private CompanyDao companyDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private WalletDao walletDao;

    @Autowired
    private ModelMapper modelMapper;


    @Bean
    public void initData() throws IOException {

        LOGGER.info("Start loading for level 2 data from "+LEVEL2_INPUT_PATH);
        File file = new File(LEVEL2_INPUT_PATH);
        ObjectMapper mapper = new ObjectMapper();
        try {
            InputOutputData dataInput = mapper.readValue(file, InputOutputData.class);

            dataInput.getWallets().forEach(w -> this.walletDao.add(this.modelMapper.map(w, Wallet.class)));

            dataInput.getCompanies().forEach(c -> this.companyDao.add(this.modelMapper.map(c, Company.class)));

            dataInput.getUsers().forEach(u -> this.userDao.add(this.modelMapper.map(u, User.class)));


            LOGGER.info("End loading data");
        } catch (IOException e) {
            LOGGER.error("Error loading file "+ LEVEL2_INPUT_PATH, e);
            throw  e;
        }
    }
}

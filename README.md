## Sent result 
* I Implemented level-1, level-2 and level-3
* For level-1 and and level-2, result can be seen at {LEVEL_X}/data/output.json 
* For level-3, it is a spring boot application for level-1 only 
    * Spring boot application
        * server port : 8080 
        * context-path : /challenge/level1/
    * Security layer managed by spring security and InMemoryAuthentication
    * User to be used 
        * username: zied
        * password: 12345678 
    * URL to be tested : 
    
|description|URL|METHOD|REQUEST CONTENT | 
|---|---|----|----|
|get balances for all users|http://localhost:8080/challenge/level1/balance/| GET| - |
|get balance by user|http://localhost:8080/challenge/level1/balance/{user_id}| GET| - |
|create distribution|http://localhost:8080/challenge/level1/balance/| POST|```{"id":2,"amount":100.0,"start_date":"2020-08-01","end_date":"2021-07-31","company_id":1,"user_id":2} ``` |
    
